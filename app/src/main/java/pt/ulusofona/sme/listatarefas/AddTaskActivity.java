package pt.ulusofona.sme.listatarefas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddTaskActivity extends AppCompatActivity {

    private EditText addTaskNameEditText;
    private EditText addQuantity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        addTaskNameEditText = this.findViewById(R.id.addTaskTextInputEditText);
        addQuantity = this.findViewById(R.id.addTaskTextInputEditQuantity);
        this.setTitle("Adicionar Item");
    }

    public void actionAddTask(View view) {
        String taskName = addTaskNameEditText.getText().toString();
        String quantity = addQuantity.getText().toString();

        if(taskName.matches("")){

            this.addTaskNameEditText.setError("POr favor inserir um item");

        }

        if(quantity.matches("")){
            this.addQuantity.setError("Por favor inserir uma quantidade");
        }

        else {

            TaskManager.getInstance().addTask(taskName);
            this.finish();
        }
    }


}
