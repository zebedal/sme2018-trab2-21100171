package pt.ulusofona.sme.listatarefas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

public class TaskListActivity extends AppCompatActivity {

    private TaskListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.taskList);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //Get data
        List<String> data = TaskManager.getInstance().getTasks();

        // specify an adapter
        mAdapter = new TaskListAdapter(data, new TaskListAdapter.onTaskItemClickListener() {
            @Override
            public void onTaskItemClick(String taskName) {
                actionNavigateToTaskDetail(taskName);
            }
        });

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }

    public void actionNavigateToAddTask(View view) {
        this.startActivity(new Intent(this,AddTaskActivity.class));
    }

    public void actionNavigateToTaskDetail(String taskName) {
        Intent intent = new Intent(this,TaskDetailActivity.class);
        intent.putExtra(TaskDetailActivity.PARAM_TASK_NAME,taskName);
        this.startActivity(intent);
    }
}
