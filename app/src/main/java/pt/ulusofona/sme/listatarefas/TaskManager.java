package pt.ulusofona.sme.listatarefas;

import java.util.ArrayList;
import java.util.List;

public class TaskManager {

    private static TaskManager _instance;

    private ArrayList<String> taskList;

    private void initTaskList(){
        this.taskList.add("Leite");
        this.taskList.add("Batatas");
        this.taskList.add("Cebolas");
        this.taskList.add("Farinha");
        this.taskList.add("Água");
        this.taskList.add("Carne Vaca");
        this.taskList.add("Bananas");

    }

    private TaskManager() {
        this.taskList = new ArrayList<String>();
        initTaskList();
    }

    public static TaskManager getInstance(){
        if(_instance ==null){
            _instance = new TaskManager();
        }
        return _instance;
    }

    public List<String> getTasks(){
        return taskList;
    }

    public void addTask(String taskName){
        this.taskList.add(taskName);
    }

    public void deleteTask(String taskName){
        this.taskList.remove(taskName);
    }

}
