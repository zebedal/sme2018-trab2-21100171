package pt.ulusofona.sme.listatarefas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class TaskDetailActivity extends AppCompatActivity {

    public static final String PARAM_TASK_NAME = "PARAM_TASK_NAME";
    private String taskName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);

        taskName = this.getIntent().getStringExtra(PARAM_TASK_NAME );
        TextView taskDetailTextView = (TextView)this.findViewById(R.id.taskDetailtextView);
        taskDetailTextView.setText(taskName );
    }

    public void actionTaskDelete(View view) {
        TaskManager.getInstance().deleteTask(taskName);
        this.finish();

    }
}
